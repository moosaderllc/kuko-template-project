if ( language == nil ) then language = {} end

language = {
	suggested_font = "NotoSans-Bold.ttf",

	language = "English",

	game_title = "Kuko Project",
	game_creator = "YOU",
	game_website = "Website.com",
	game_year = "2019",
	game_version = "v0.1",

	mainmenu_play = "Play",
	mainmenu_options = "Options",
	mainmenu_help = "Help",
	mainmenu_exit = "Exit",
    mainmenu_back = "Back",
    mainmenu_character_hub = "Character Hub",

    options_sound_volume = "Sound volume",
    options_music_volume = "Music volume",
}
