assets = {
    { name = "background",      path = "content/graphics/ui/background.png" },
    { name = "button",          path = "content/graphics/ui/button1_bg.png" },
    { name = "buttonsq",        path = "content/graphics/ui/button2_bg.png" },
    { name = "volume_slider",   path = "content/graphics/ui/volume_slider.png" },
    { name = "icons",           path = "content/graphics/ui/menu_icons.png" }
}

screenWidth = 1280
screenHeight = 720

buttonWidth = 300
buttonHeight = 50
buttonR = 0
buttonG = 0
buttonB = 0

elements = {
    -- Background image
    {
        ui_type = "image", id = "background",
        texture_id = "background",
        x = 0, y = 0,
        width = screenWidth, height = screenHeight
    },

    -- Sound volume control
    {
        ui_type = "label", id = "lbl_sound_vol", text_id = "options_sound_volume",
        x = 400, y = 100, width = 0, height = 50,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = 0, shadow_g = 0, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1
    },

    {
        ui_type = "button", id = "btn_minus_sound", background_texture_id = "buttonsq",
        x = 400, y = 100+50, width = 50, height = 50,
        icon_texture_id = "icons",
        icon_frame_x = 6*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 5, icon_y = 5,
        icon_width = 40, icon_height = 40,
    },

    {
        ui_type = "button", id = "btn_plus_sound", background_texture_id = "buttonsq",
        x = 400+270+70, y = 100+50, width = 50, height = 50,
        icon_texture_id = "icons",
        icon_frame_x = 7*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 5, icon_y = 5,
        icon_width = 40, icon_height = 40,
    },

    {
        ui_type = "button", id = "sound_slider", background_texture_id = "volume_slider",
        x = 400+60, y = 100+55, width = 270, height = 40,
        frame_x = 0, frame_y = 40*3, frame_width = 294, frame_height = 40,
    },

    -- Music volume control
    {
        ui_type = "label", id = "lbl_music_vol", text_id = "options_music_volume",
        x = 400, y = 300, width = 0, height = 50,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = 0, shadow_g = 0, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1
    },

    {
        ui_type = "button", id = "btn_minus_music", background_texture_id = "buttonsq",
        x = 400, y = 300+50, width = 50, height = 50,
        icon_texture_id = "icons",
        icon_frame_x = 6*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 5, icon_y = 5,
        icon_width = 40, icon_height = 40,
    },

    {
        ui_type = "button", id = "btn_plus_music", background_texture_id = "buttonsq",
        x = 400+270+70, y = 300+50, width = 50, height = 50,
        icon_texture_id = "icons",
        icon_frame_x = 7*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 5, icon_y = 5,
        icon_width = 40, icon_height = 40,
    },

    {
        ui_type = "button", id = "music_slider", background_texture_id = "volume_slider",
        x = 400+60, y = 300+55, width = 270, height = 40,
        frame_x = 0, frame_y = 40*3, frame_width = 294, frame_height = 40,
    },

    -- Sidebar
    {
        ui_type = "label", id = "lbl_header",
        text_id = "mainmenu_options",
        x = 10, y = 10,
        width = 0,
        height = 60,
        font_id = "header",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255,
        shadow_r = 0, shadow_g = 0, shadow_b = 0, shadow_a = 255, shadow_offset_x = 5, shadow_offset_y = 5, use_shadow = 1
    },

    {
        ui_type = "button", id = "btn_back",
        background_texture_id = "button",
        x = 10, y = 600,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 5, icon_y = 5,
        icon_width = 40, icon_height = 40,
        text_id = "mainmenu_back",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5
    },
}
