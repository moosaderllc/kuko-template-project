assets = {
    { name = "background",  path = "content/graphics/ui/background.png" },
    { name = "button",      path = "content/graphics/ui/button1_bg.png" },
    { name = "icons",       path = "content/graphics/ui/menu_icons.png" },
    { name = "socialmedia", path = "content/graphics/ui/social_icons.png" },
}

screenWidth = 1280
screenHeight = 720

buttonWidth = 300
buttonHeight = 50
buttonR = 0
buttonG = 0
buttonB = 0

elements = {
    -- Background image
    {
        ui_type = "image", id = "background",
        texture_id = "background",
        x = 0, y = 0,
        width = screenWidth, height = screenHeight
    },

    -- Main menu text
    {
        ui_type = "label", id = "lbl_title",
        text_id = "game_title",
        x = 600, y = 50,
        width = 0,
        height = 100,
        font_id = "header",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255,
        shadow_r = 0, shadow_g = 0, shadow_b = 0, shadow_a = 255, shadow_offset_x = 5, shadow_offset_y = 5, use_shadow = 1
    },

    -- Options buttons
    {
        ui_type = "button", id = "btn_character_hub",
        background_texture_id = "button",
        x = 10, y = 100,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 0, icon_frame_y = 0,
        icon_frame_width = 40, icon_frame_height = 45,
        icon_x = 7, icon_y = 7,
        icon_width = 36, icon_height = 36,
        text_id = "mainmenu_character_hub",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5,
    },

    {
        ui_type = "button", id = "btn_play",
        background_texture_id = "button",
        x = 10, y = 200,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 2*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 7, icon_y = 7,
        icon_width = 36, icon_height = 36,
        text_id = "mainmenu_play",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5
    },

    {
        ui_type = "button", id = "btn_options",
        background_texture_id = "button",
        x = 10, y = 300,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 3*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 7, icon_y = 7,
        icon_width = 36, icon_height = 36,
        text_id = "mainmenu_options",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5
    },

    {
        ui_type = "button", id = "btn_help",
        background_texture_id = "button",
        x = 10, y = 400,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 4*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 7, icon_y = 7,
        icon_width = 36, icon_height = 36,
        text_id = "mainmenu_help",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5
    },

    {
        ui_type = "button", id = "btn_exit",
        background_texture_id = "button",
        x = 10, y = 600,
        width = buttonWidth, height = buttonHeight,
        icon_texture_id = "icons",
        icon_frame_x = 5*45, icon_frame_y = 0,
        icon_frame_width = 45, icon_frame_height = 45,
        icon_x = 7, icon_y = 7,
        icon_width = 36, icon_height = 36,
        text_id = "mainmenu_exit",
        font_id = "main",
        font_r = buttonR, font_g = buttonG, font_b = buttonB, font_a = 255,
        shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255,
        shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1,
        pad_x1 = 50, pad_x2 = 5, pad_y1 = 5, pad_y2 = 5
    },

    {
        ui_type = "button", id = "btn_website", background_texture_id = "socialmedia",
        x = 1280-300, y = 720-100, width = 84, height = 84,
        frame_x = 84*2, frame_y = 0, frame_width = 84, frame_height = 84
    },

    {
        ui_type = "button", id = "btn_facebook", background_texture_id = "socialmedia",
        x = 1280-200, y = 720-100, width = 84, height = 84,
        frame_x = 0, frame_y = 0, frame_width = 84, frame_height = 84
    },

    {
        ui_type = "button", id = "btn_twitter", background_texture_id = "socialmedia",
        x = 1280-100, y = 720-100, width = 84, height = 84,
        frame_x = 84, frame_y = 0, frame_width = 84, frame_height = 84
    },
}
