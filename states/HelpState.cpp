#include "HelpState.hpp"
#include <cstdio>
#include <cstdlib>

#include "../kuko/managers/ImageManager.hpp"
#include "../kuko/managers/InputManager.hpp"
#include "../kuko/managers/FontManager.hpp"
#include "../kuko/managers/LanguageManager.hpp"
#include "../kuko/managers/SoundManager.hpp"
#include "../kuko/managers/ConfigManager.hpp"
#include "../kuko/managers/LuaManager.hpp"
#include "../kuko/utilities/Logger.hpp"
#include "../kuko/base/Application.hpp"
#include "../kuko/utilities/StringUtil.hpp"
#include "../kuko/utilities/Platform.hpp"
#include "../kuko/utilities/Messager.hpp"

HelpState::~HelpState()
{
}

HelpState::HelpState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void HelpState::Setup()
{
    m_state = "";
    kuko::IState::Setup();
    //kuko::SoundManager::PlayMusic( "title", true );

    Logger::Debug( "Menu Transition", "HelpState::SetupMenu_HelpState" );
    menuManager.LoadMenuScript( m_contentPath + "menus/help_screen.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}

void HelpState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void HelpState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void HelpState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId == "btn_back" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "title";
        }
    }

    menuManager.Update();
}

void HelpState::Draw()
{
    menuManager.Draw();
}
