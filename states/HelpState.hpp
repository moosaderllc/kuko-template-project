#ifndef _HELPSTATE
#define _HELPSTATE

#include "../kuko/states/IState.hpp"
#include "../kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>
#include <stack>

class HelpState : public kuko::IState
{
    public:
    HelpState( const std::string& contentPath );
    virtual ~HelpState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    void AdjustVolume( const std::string& buttonName );

    std::string m_state;
    std::stack< std::string > m_stateStack;
    std::vector< std::string > m_lstSaveGames;

    std::string m_contentPath;

    const int TOTAL_SAVES = 8;
};

#endif
