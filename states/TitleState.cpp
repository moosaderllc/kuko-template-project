#include "TitleState.hpp"
#include <cstdio>
#include <cstdlib>

#include "../kuko/managers/ImageManager.hpp"
#include "../kuko/managers/InputManager.hpp"
#include "../kuko/managers/FontManager.hpp"
#include "../kuko/managers/LanguageManager.hpp"
#include "../kuko/managers/SoundManager.hpp"
#include "../kuko/managers/ConfigManager.hpp"
#include "../kuko/managers/LuaManager.hpp"
#include "../kuko/utilities/Logger.hpp"
#include "../kuko/base/Application.hpp"
#include "../kuko/utilities/StringUtil.hpp"
#include "../kuko/utilities/Platform.hpp"
#include "../kuko/utilities/Messager.hpp"

TitleState::~TitleState()
{
}

TitleState::TitleState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void TitleState::Setup()
{
    m_state = "";
    kuko::IState::Setup();
    //kuko::SoundManager::PlayMusic( "title", true );

    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_TitleScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/title_screen.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}

void TitleState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void TitleState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void TitleState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId == "btn_character_hub" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
        }
        else if ( clickedButtonId == "btn_play" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
        }
        else if ( clickedButtonId == "btn_options" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "options";
        }
        else if ( clickedButtonId == "btn_help" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "help";
        }
        else if ( clickedButtonId == "btn_exit" )
        {
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "quit";
        }
        else if ( clickedButtonId == "btn_website" )
        {
            Platform::OpenUrl( kuko::ConfigManager::GetOption( "website" ) );
        }
        else if( clickedButtonId == "btn_facebook" )
        {
            Platform::OpenUrl( kuko::ConfigManager::GetOption( "facebook" ) );
        }
        else if( clickedButtonId == "btn_twitter" )
        {
            Platform::OpenUrl( kuko::ConfigManager::GetOption( "twitter" ) );
        }
    }

    menuManager.Update();
}

void TitleState::Draw()
{
    menuManager.Draw();
}
